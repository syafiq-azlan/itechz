angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $location, $state) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {

    var usrName = $scope.loginData.usrName;
    var usrPass = $scope.loginData.usrPass;

    console.log('Doing login', usrName);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };


  $scope.doReport = function()
  {
    console.log("ree");
$location.path("#/app/report");
  }

  $scope.doLogout = function() 
  {
    $state.go('tabs6.login');
  }

  $scope.goEdit = function()
  {
    $state.go('tabs9.profile');
  }

})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})


.controller('MyTicketCtrl', function($scope, $location, $state, $window, $ionicLoading, $ionicPopup, $ionicSideMenuDelegate, $ionicScrollDelegate, $http, $stateParams) 
{ 
  // if ($scope.$root.username == undefined || window.localStorage.getItem("username") == null)
  // {
  //   $state.go('loginTab.login');
  //   console.log("login as login ", window.localStorage.getItem("username"));
  // }
  
  // else{
  
  //   //http://onewoorks.com/idesk-api/tickets.json
  //  $http.get("http://onewoorks.com/idesk-api/tickets.json?status=assigned").
  //  then(function(response, data)
  //   {
  //   $scope.ticketData = response.data;
  //   console.log("Data : ", $scope.ticketData);

  //   })
  // }
            $scope.emptyIcon = true;

console.log("my ticket ctrl");
    //http://onewoorks.com/idesk-api/tickets.json

    var user_id = 2;
   $http.get("https://syafiqevo.000webhostapp.com/itechz/getTicketMy.php?user_id="+user_id).
   then(function(response, data)
    {
    if (response.data != undefined)
    {
          $scope.ticketData = response.data;
          console.log("Data : ", $scope.ticketData);
          $scope.emptyIcon = true;
    }
    else
    {
      console.log("errorrr");
                $scope.emptyIcon = false;
    }
    })  

  //Ticket Details
  $scope.goDetails = function(ID) {
      $scope.$root.current_ticket_id = ID;  

   $state.go('tab1.goDetails');
  //$location.path('/app/goDetails');
  };

  //Control ng-change input tu filter search
    $scope.searchTicket = function (myTicket) {
    
    $scope.$root.searchFor = myTicket.ticketSubject;
    };
  
  //Search function
  $scope.openSearch = function() {


    if ($scope.active == null)
    {
          $scope.$root.searchFor = undefined;
      $scope.active = 1;
    }

    else if ($scope.active == 1)
    {
      if ($scope.$root.searchFor == undefined)
      {
      $scope.active == null;
      }
      else{
      $scope.$root.searchForStatus = "myTicket";
      $scope.active = null;
      $state.go('tab4.goSearch');
      console.log("My tickt search for : ", $scope.$root.searchFor);
      }
    }
  }

//searchh

$scope.myTicket = {};
    $scope.searchTicket = function (myTicket) {
    
    $scope.$root.searchFor = myTicket.ticketSubject;
    
    //console.log("My tickt search for : ", $scope.searchFor);
    };
  
  //Search function
  $scope.openSearch = function() {


    if ($scope.active == null)
    {
          $scope.$root.searchFor = undefined;
      $scope.active = 1;
      //$scope.$root.searchFor = "";
    }

    else if ($scope.active == 1)
    {
      if ($scope.$root.searchFor == undefined)
      {
      $scope.active == null;
      }
      else{
      $scope.$root.searchForStatus = "myTicket";
      $scope.active = null;
      $state.go('tabs3.goSearch');
      console.log("My tickt search for : ", $scope.$root.searchFor);
      //$scope.$root.searchFor = "";
      }
    }
  }

//end search
  $scope.doRefresh = function() 
  { 
        var user_id = 2;

   $http.get("https://syafiqevo.000webhostapp.com/itechz/getTicketMy.php?user_id="+user_id).
   success(function(response)
    {
    $scope.ticketData = response;
    })
    .error(function()
    {
      $ionicLoading.show({ template: 'Refresh Fail', noBackdrop: true, duration: 2500 });
    })
    
    $scope.$broadcast('scroll.refreshComplete');
  };
  

})

.controller('TicketDetailCtrl', function($scope, $state, $http, $ionicLoading) {
  console.log("details");
   $ionicLoading.show({
      template: 'Loading...',
    noBackdrop : true,
    }); 

  $http.get("https://syafiqevo.000webhostapp.com/itechz/getTicketDetails.php?id="+$scope.$root.current_ticket_id).
   then(function(response, data)
    {

          $scope.ticketData = response.data;
          $ionicLoading.hide();
          console.log("Data details : ", $scope.ticketData);

})

     //Reply ticket
  $scope.goReply = function() {
    console.log("replyy");
  $state.go('tabs2.goReply');
  };
  

})

.controller('ReplyCtrl', function($scope, $state, $http, $ionicLoading, $ionicPopup) {

  $scope.ticketDetailData = {};

$scope.ticketResponse = function()
{
    var status = $scope.ticketDetailData.ticket_status;
  var body = $scope.ticketDetailData.ticket_body;

  if (status || body != undefined)
  {
$ionicPopup.alert({
           title: 'Success',
           template: 'Your response have been recorded'
         })

        .then(function(res) {
      $state.go('app.my_ticket');
   });

  
  }
  else
  {
      $ionicLoading.show({ 
  template: 'Please complete your response', 
  noBackdrop: true, 
  duration: 1500 ,
  delay : 0
  }); 
  }


  console.log(status + " ---- " + body);
}



})

.controller('SearchCtrl', function($scope, $state, $http) {

console.log("search ape : ",$scope.$root.searchFor);
    $scope.searchKey = $scope.$root.searchFor;

  $http.get("https://syafiqevo.000webhostapp.com/itechz/searchTicket.php?keyword="+$scope.$root.searchFor).
      then(function(response, data)
    {
    $scope.hideResultFound = false;
    $scope.matchFound = response.data.length;
        $scope.ticketData = response.data;
        console.log("src data:", $scope.ticketData);
    })

  //Ticket Details
  $scope.goDetails = function(ID) {
      $scope.$root.current_ticket_id = ID;  

   $state.go('tab1.goDetails');
  //$location.path('/app/goDetails');
  }

})

.controller('ReportCtrl', function($scope, $state, $ionicPopup, $ionicLoading) {
  console.log("report ctrll");

$scope.ticketDetailData = {};

  $scope.goReport = function()
  {
  var body = $scope.ticketDetailData.ticket_body;

  if (body != undefined)
  {
    $ionicPopup.alert({
               title: 'Success',
               template: 'Your report have been recorded'
             })

            .then(function(res) {
          $state.go('app.my_ticket');
       });  
  }
  else
  {
        $ionicLoading.show({ 
    template: 'Please complete your report', 
    noBackdrop: true, 
    duration: 1500 ,
    delay : 0
    }); 
  }  
}
})

.controller('LoginCtrl', function($scope, $state, $ionicLoading, $ionicPopup) {

$scope.loginData = {};

  $scope.doLogin = function()
  {
    var usrName = $scope.loginData.usrName;
    var usrPass = $scope.loginData.usrPass;

    console.log('Doing login', usrName);

    if (usrName == "tech1" && usrPass == "abc123")
    {
      console.log('technician masuk');
      $state.go('app.my_ticket');
      $scope.$root.techTab = false;
      $scope.$root.usrTab = true;
    }
    else if(usrName == "B031610058" && usrPass == "abc123")
    {
      $scope.$root.techTab = true;
      $scope.$root.usrTab = false;
     $state.go('app.replyUser');
    }
    else
    {
       $ionicPopup.alert({
               title: 'Login Failed',
               template: 'Incorrect username or password'
             });  
    } 
  }

  $scope.doRegister = function()
  {
    $state.go('tabs8.register');
  }
})

.controller('HistoryCtrl', function($scope, $ionicPopup){

$scope.goDetails = function()
{

  var status = "Solved";
  var location = "AI Lab 3";
  var date = "29/12/2016 10:30 PM";
  var description = "Wayar mouse putus";

   $ionicPopup.alert({
     title: 'Ticket Details',
     template: 'Status: '+status+' <br /> Location: '+location+'<br /> Date: '+date+' <br/> Description: '+description
   });  
}


})

.controller('ReplyUserCtrl', function($scope, $state, $ionicPopup) {

  $scope.userData = {};

  $scope.doSubmit = function()
  {
    var location = $scope.userData.location;
    var body = $scope.userData.body;

    if(location != undefined || body != undefined)
    {
      $ionicPopup.alert({
               title: 'Success',
               template: 'Your report have been recorded'
      });      
    }
    else
    {
            $ionicPopup.alert({
               title: 'Fail',
               template: 'Please chek your report details'
      });    
    }
  }

})

.controller('RegisterCtrl', function($scope, $state, $ionicPopup) {

$scope.regData = {};


$scope.doRegister = function()
{
  if ($scope.regData.username != undefined)
  {

      $ionicPopup.alert({
               title: 'Success',
               template: 'Your have succesfuly registered'
             })

            .then(function(res) {
          $state.go('tabs6.login');
       });  
  }

  else
  {
       $ionicPopup.alert({
               title: 'Fail',
               template: 'Please check your registration details'
             });   
  }
}

})


.controller('ProfileCtrl', function($scope, $state, $ionicPopup) {

  $scope.profileData = {};

  $scope.doEdit = function()
  {
    var gender = $scope.profileData.gender;
    var username = $scope.profileData.username;
    var email = $scope.profileData.email;
    var password = $scope.profileData.password;

    if (gender != undefined || username != undefined || email != undefined || password != undefined)
    {
        $ionicPopup.alert({
               title: 'Success',
               template: 'Your profile have been succesfuly updated'
             });       
    }
    else
    {
          $ionicPopup.alert({
               title: 'Fail',
               template: 'Please check you details'
             });       
    }

  }
})

.controller('ClosedCtrl', function($scope, $state, $http) {
})
.controller('OpenCtrl', function($scope, $state, $http) {
})
.controller('OverdueCtrl', function($scope, $state, $http) {
})
.controller('AnsweredCtrl', function($scope, $state, $http) {
})
.controller('PlaylistCtrl', function($scope, $stateParams) {
});
