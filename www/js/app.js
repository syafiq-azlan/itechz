// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

    .state('tab1', {
    url: "/tab1",
    abstract: true,
    templateUrl: "templates/tab-details.html",
  })

    .state('tab1.goDetails', {
    url: "/goDetails",
      cache: false,
    views: {
      'goDetails': {
        templateUrl: "templates/ticket-details.html",
        controller: 'TicketDetailCtrl'
      }
    }
  })

    .state('tabs2', {
    url: "/tabs2",
    abstract: true,
    templateUrl: "templates/tabs2.html",
  })

    .state('tabs2.goReply', {
    url: "/goReply",
      cache: false,
    views: {
      'goReply': {
        templateUrl: "templates/ticket-reply.html",
        controller: 'ReplyCtrl'
      }
    }
  })

    .state('tabs3', {
    url: "/tabs3",
    abstract: true,
    templateUrl: "templates/tabs3.html",
  })

    .state('tabs3.goSearch', {
    url: "/goSearch",
      cache: false,
    views: {
      'goSearch': {
        templateUrl: "templates/ticket-search.html",
        controller: 'SearchCtrl'
      }
    }
  })

    .state('tabs9', {
    url: "/tabs9",
    abstract: true,
    templateUrl: "templates/tabs9.html",
  })

    .state('tabs9.profile', {
    url: "/profile",
      cache: false,
    views: {
      'profile': {
        templateUrl: "templates/profile.html",
        controller: 'ProfileCtrl'
      }
    }
  })

  .state('app.my_ticket', {
    url: "/my_ticket",
    views: {
      'tab-my_ticket': {
        templateUrl: "templates/my_ticket.html",
        controller :'MyTicketCtrl'
      }
    }
  })

  .state('app.answered', {
    url: "/answered",
    views: {
      'tab-answered': {
        templateUrl: "templates/answered.html"
      }
    }
  })

  .state('app.overdue', {
    url: "/overdue",
    views: {
      'tab-overdue': {
        templateUrl: "templates/overdue.html"
      }
    }
  })

  .state('app.closed', {
    url: "/closed",
    views: {
      'tab-closed': {
        templateUrl: "templates/closed.html"
      }
    }
  })

  .state('app.open', {
    url: "/open",
    views: {
      'tab-open': {
        templateUrl: "templates/open.html"
      }
    }
  })

  .state('app.browse', {
    url: "/browse",
    views: {
      'tab-browse': {
        templateUrl: "templates/browse.html"
      }
    }
  })

    .state('tabs4', {
    url: "/tabs4",
    abstract: true,
    templateUrl: "templates/tabs4.html",
  })

    .state('tabs4.report', {
    url: "/report",
    views: {
      'report': {
        templateUrl: "templates/report.html",
        controller: 'ReportCtrl'
      }
    }
  })

    .state('tabs5', {
    url: "/tabs5",
    abstract: true,
    templateUrl: "templates/tabs5.html",
  })

    .state('tabs5.about', {
    url: "/about",
    views: {
      'about': {
        templateUrl: "templates/about.html"
      }
    }
  })

    .state('tabs8', {
    url: "/tabs8",
    abstract: true,
    templateUrl: "templates/tabs8.html",
  })

    .state('tabs8.register', {
    url: "/register",
    views: {
      'register': {
        templateUrl: "templates/register.html",
        controller: 'RegisterCtrl'
      }
    }
  })

  .state('app.playlists', {
    url: "/playlists",
    views: {
      'tab-playlists': {
        templateUrl: "templates/playlists.html",
        controller: 'PlaylistsCtrl'
      }
    }
  })

    .state('app.single', {
      url: "/playlists/:playlistId",
      views: {
        'tab-playlists': {
          templateUrl: "templates/playlist.html",
          controller: 'PlaylistCtrl'
        }
      }
    })

        .state('app.replyUser', {
      url: "/replyUser",
      views: {
        'tab-replyUser': {
          templateUrl: "templates/replyUser.html",
          controller: 'ReplyUserCtrl'
        }
      }
    })


        .state('app.history', {
      url: "/history",
      views: {
        'tab-history': {
          templateUrl: "templates/history.html",
          controller: 'HistoryCtrl'
        }
      }
    })

    .state('tabs6', {
    url: "/tabs6",
    abstract: true,
    templateUrl: "templates/tabs6.html",
  })

      .state('tabs6.login', {
    url: "/login",
    views: {
      'login': {
        templateUrl: "templates/login.html",
        controller: 'LoginCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tabs6/login');
});
